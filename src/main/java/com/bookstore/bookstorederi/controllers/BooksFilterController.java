package com.bookstore.bookstorederi.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.bookstorederi.dtomodel.BookDTO;
import com.bookstore.bookstorederi.model.Book;
import com.bookstore.bookstorederi.repositories.BookRepository;

@RestController
@RequestMapping("/api/book")
public class BooksFilterController {
	@Autowired
	BookRepository booksRepository;
	ModelMapper modelMapper = new ModelMapper();
	//filter by year
	@GetMapping("/filtering/{year}")
	public Map<String, Object> filterBookByYear(@PathVariable(value = "year") Long year) {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Book> listBook = booksRepository.findAll();
		Calendar cal = Calendar.getInstance();
		
		List<BookDTO> listBookDTO = new ArrayList<BookDTO>();
		List<BookDTO> listBookDTOByYear = new ArrayList<BookDTO>();
		for(Book book : listBook) {
			BookDTO bookDTO = modelMapper.map(book, BookDTO.class);
			listBookDTO.add(bookDTO);
		}
		
		for(BookDTO bookDTO : listBookDTO) {
			cal.setTime(bookDTO.getReleaseDate());
			if(cal.get(Calendar.YEAR) == year) {
				listBookDTOByYear.add(bookDTO);
			}
		}
		result.put("Status", 200);
		result.put("Message", "Filter book by year succesful");
		result.put("Data", listBookDTOByYear);
		return result;
	}
	
	
}
