package com.bookstore.bookstorederi.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.bookstorederi.dtomodel.PaperQualityDTO;
import com.bookstore.bookstorederi.model.PaperQuality;
import com.bookstore.bookstorederi.repositories.PaperQualityRepository;
@RestController
@RequestMapping("/api/paperQuality")
public class PaperQualityController {

	
	@Autowired
	PaperQualityRepository paperQualityRepository;
	ModelMapper modelMapper = new ModelMapper();
	//create
	@PostMapping("/create")
	public Map<String , Object> createPaperQuality(@Valid @RequestBody PaperQualityDTO paperQualityDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		PaperQuality paperQuality = modelMapper.map(paperQualityDTO, PaperQuality.class);
		paperQualityRepository.save(paperQuality);
		
		result.put("Status", 200);
		result.put("Message", "create data paper quality succesful");
		result.put("Data", paperQuality);
		
		
		return result;
	}
	
	//read all
	@GetMapping("readAll")
	public Map<String, Object> getAllPaperQuality(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<PaperQuality> listPaperQuality = paperQualityRepository.findAll();
		List<PaperQualityDTO> listPaperQualityDTO = new ArrayList<PaperQualityDTO>();
		for(PaperQuality paperQuality : listPaperQuality) {
			PaperQualityDTO paperQualityDTO = modelMapper.map(paperQuality, PaperQualityDTO.class);
			listPaperQualityDTO.add(paperQualityDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read all data paper quality succesful");
		result.put("Data", listPaperQualityDTO);
		
		
		return result;
		
	}
	
	//read by id
	@GetMapping("/read/{id}")
	public Map<String, Object> getPaperQualityById(@PathVariable(value = "id") Long paperQualityID){
		Map<String, Object> result = new HashMap<String, Object>();
		PaperQuality paperQualityByID = paperQualityRepository.findById(paperQualityID).get();
		PaperQualityDTO paperQualityDTOByID = modelMapper.map(paperQualityByID, PaperQualityDTO.class);
		result.put("Status", 200);
		result.put("Message", "Read data paper quality by id succesful");
		result.put("Data", paperQualityDTOByID);
		return result;
	}
	
	//update
	@PutMapping("/update/{id}")
	public Map<String, Object> updatePaperQuality(@PathVariable(value = "id") Long paperQualityID, @Valid @RequestBody PaperQualityDTO paperQualityDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		PaperQuality paperQuality = paperQualityRepository.findById(paperQualityID).get();
		paperQuality = modelMapper.map(paperQualityDTO, PaperQuality.class);
		paperQuality.setQualityId(paperQualityID);
		paperQualityRepository.save(paperQuality);
		result.put("Status", 200);
		result.put("Message", "Update data paper quality succesful");
		result.put("Data", paperQuality);
		return result;
	}
	
	
	//delete
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deletePaperQuality(@PathVariable(value = "id") Long paperQualityID){
		Map<String, Object> result = new HashMap<String, Object>();
		PaperQuality paperQuality = paperQualityRepository.findById(paperQualityID).get();
		paperQualityRepository.delete(paperQuality);
		result.put("Status", 200);
		result.put("Message", "Delete data paper quality succesful");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}
	
}
