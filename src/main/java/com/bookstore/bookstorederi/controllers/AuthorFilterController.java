package com.bookstore.bookstorederi.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.bookstorederi.dtomodel.AuthorDTO;
import com.bookstore.bookstorederi.model.Author;
import com.bookstore.bookstorederi.repositories.AuthorRepository;

@RestController
@RequestMapping("/api/author")
public class AuthorFilterController {
	@Autowired
	AuthorRepository authorRepository;
	ModelMapper modelMapper = new ModelMapper();
	//fillter by country
	@GetMapping("/filtering/{country}")
	public Map<String, Object> getAuthorByCountry(@PathVariable(value = "country") String country){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Author> listAuthor = authorRepository.findAll();
		List<AuthorDTO> listAuthorDTO = new ArrayList<AuthorDTO>();
		List<AuthorDTO> listAuthorDTOByCountry = new ArrayList<AuthorDTO>();
		for(Author author : listAuthor) {
			AuthorDTO authorDTO = modelMapper.map(author, AuthorDTO.class);
			listAuthorDTO.add(authorDTO);
		}
		
		for(AuthorDTO authorDTO : listAuthorDTO) {
				if(authorDTO.getCountry().equalsIgnoreCase(country)) {
				listAuthorDTOByCountry.add(authorDTO);
			}
		}
		result.put("Status", 200);
		result.put("Message", "Filter author by country succesful");
		result.put("Data", listAuthorDTOByCountry);
		return result;
	}
}
