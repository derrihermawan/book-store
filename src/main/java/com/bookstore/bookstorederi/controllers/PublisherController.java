package com.bookstore.bookstorederi.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.bookstorederi.dtomodel.PublisherDTO;
import com.bookstore.bookstorederi.model.Publisher;
import com.bookstore.bookstorederi.repositories.PublisherRepository;

@RestController
@RequestMapping("/api/publisher")
public class PublisherController {
	@Autowired
	PublisherRepository publisherRepository;
	ModelMapper modelMapper = new ModelMapper();
	
	//create publisher
	@PostMapping("/create")
	public Map<String, Object> createPublisher(@Valid @RequestBody PublisherDTO publisherDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Publisher publisher = modelMapper.map(publisherDTO, Publisher.class);
		publisherRepository.save(publisher);
		result.put("Status", 200);
		result.put("Message", "Create data publisher succesful");
		result.put("Data", publisher);
		
		return result;
	}
	
	//read all
	@GetMapping("/readAll")
	public Map<String, Object> getAllPublisher(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Publisher> listPublisher = publisherRepository.findAll();
		List<PublisherDTO> listPublisherDTO = new ArrayList<PublisherDTO>();
		for(Publisher publisher : listPublisher) {
			PublisherDTO publisherDTO = modelMapper.map(publisher, PublisherDTO.class);
			listPublisherDTO.add(publisherDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read all data publisher succesful");
		result.put("Data", listPublisherDTO);
		return result;
	}
	
	//read by id
	@GetMapping("/read/{id}")
	public Map<String, Object> getPublisherById(@PathVariable(value = "id") Long publisherID){
		Map<String, Object> result = new HashMap<String, Object>();
		Publisher publisher = publisherRepository.findById(publisherID).get();
		PublisherDTO publisherDTO = modelMapper.map(publisher, PublisherDTO.class);
		result.put("Status", 200);
		result.put("Message", "Read data publisher by id succesful");
		result.put("Data", publisherDTO);
		return result;
	}
	
	//update publisher
	@PutMapping("/update/{id}")
	public Map<String, Object> updatePublisher (@PathVariable(value = "id") Long publisherID, @Valid @RequestBody PublisherDTO publisherDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Publisher publisher = publisherRepository.findById(publisherID).get();
		publisher = modelMapper.map(publisherDTO, Publisher.class);
		publisher.setPublisherId(publisherID);
		publisherRepository.save(publisher);
		result.put("Status", 200);
		result.put("Message", "Update data publisher succesful");
		result.put("Data", publisher);
		return result;
	}
	
	//delete publisher
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deletePublisher (@PathVariable(value = "id") Long publisherID){
		Map<String, Object> result = new HashMap<String, Object>();
		Publisher publisher = publisherRepository.findById(publisherID).get();
		publisherRepository.delete(publisher);
		result.put("Status", 200);
		result.put("Message", "Update data publisher succesful");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}
	
}
