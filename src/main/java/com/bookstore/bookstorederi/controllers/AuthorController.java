package com.bookstore.bookstorederi.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.bookstorederi.dtomodel.AuthorDTO;
import com.bookstore.bookstorederi.model.Author;
import com.bookstore.bookstorederi.repositories.AuthorRepository;

@RestController
@RequestMapping("/api/author")
public class AuthorController {
	
//	private ModelMapper modelMapper() {
//		ModelMapper newModelMapper = new ModelMapper();
//		return newModelMapper;
//	}
	
	@Autowired
	AuthorRepository authorRepository;
	ModelMapper modelMapper = new ModelMapper();

	//readAll author
	@GetMapping("/readAll")
	public Map<String, Object> getAllAuthor(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Author> listAuthor = authorRepository.findAll();
		List<AuthorDTO> listAuthorDTO = new ArrayList<AuthorDTO>();
		for(Author author : listAuthor) {
			AuthorDTO authorDTO = modelMapper.map(author, AuthorDTO.class);
			listAuthorDTO.add(authorDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read Data Author Succesful");
		result.put("Data", listAuthorDTO);
		return result;
	}
	
	//create author
	@PostMapping("/create")
	public Map<String, Object> createAuthor(@Valid @RequestBody AuthorDTO authorDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Author author = modelMapper.map(authorDTO, Author.class);
		authorRepository.save(author);
		result.put("Status", 200);
		result.put("Message", "Create Data Author Succesful");
		result.put("Data", author);
		return result;
	}
	
	//read author by id
	@GetMapping("/read/{id}")
	public Map<String, Object> getAuthorById(@PathVariable(value = "id") Long authorID){
		Map<String, Object> result = new HashMap<String, Object>();
		Author author = authorRepository.findById(authorID).get();
		AuthorDTO authorDTO = modelMapper.map(author, AuthorDTO.class);
		result.put("Status", 200);
		result.put("Message", "Read Data Author Succesful");
		result.put("Data", authorDTO);
		return result;
	}
	
	//update author
	@PutMapping("/update/{id}")
	public Map<String, Object> updateAuthor(@PathVariable(value = "id") Long authorID, @Valid @RequestBody AuthorDTO authorDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Author authorByID = authorRepository.findById(authorID).get();
		authorByID = modelMapper.map(authorDTO, Author.class);
		authorByID.setAuthorId(authorID);
		authorRepository.save(authorByID);
		result.put("Status", 200);
		result.put("Message", "Update Data Author Succesful");
		result.put("Data", authorByID);
		return result;
	}
	
	//delete author
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteAuthor(@PathVariable(value = "id") Long authorID){
		Map<String, Object> result = new HashMap<String, Object>();
		Author author = authorRepository.findById(authorID).get();
		authorRepository.delete(author);
		result.put("Status", 200);
		result.put("Message", "Delete Data Author Succesful");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}
	
}
