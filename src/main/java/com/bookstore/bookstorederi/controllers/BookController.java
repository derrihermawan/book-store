package com.bookstore.bookstorederi.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.bookstorederi.dtomodel.AuthorDTO;
import com.bookstore.bookstorederi.dtomodel.BookDTO;
import com.bookstore.bookstorederi.dtomodel.CategoryDTO;
import com.bookstore.bookstorederi.dtomodel.PaperQualityDTO;
import com.bookstore.bookstorederi.dtomodel.PublisherDTO;
import com.bookstore.bookstorederi.interfaces.CalculationBookBaseProductionInterface;
import com.bookstore.bookstorederi.model.Author;
import com.bookstore.bookstorederi.model.Book;
import com.bookstore.bookstorederi.model.Category;
import com.bookstore.bookstorederi.model.PaperQuality;
import com.bookstore.bookstorederi.model.Publisher;
import com.bookstore.bookstorederi.repositories.AuthorRepository;
import com.bookstore.bookstorederi.repositories.BookRepository;
import com.bookstore.bookstorederi.repositories.CategoryRepository;
import com.bookstore.bookstorederi.repositories.PaperQualityRepository;
import com.bookstore.bookstorederi.repositories.PublisherRepository;

@RestController
@RequestMapping("/api/book")
public class BookController implements CalculationBookBaseProductionInterface{
	@Autowired
	BookRepository bookRepository;
	@Autowired
	AuthorRepository authorRepository;
	@Autowired
	CategoryRepository categoryRepository;
	@Autowired
	PublisherRepository publisherRepository;
	@Autowired
	PaperQualityRepository paperQualityRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	public Double baseProductionCalculating(Long idPublisher) {
		Publisher publisher = publisherRepository.findById(idPublisher).get();
		PublisherDTO publisherDTO = modelMapper.map(publisher, PublisherDTO.class);
		PaperQuality paperQuality = paperQualityRepository.findById(publisherDTO.getPaperQuality().getQualityId()).get();
		PaperQualityDTO paperQualityDTO = modelMapper.map(paperQuality, PaperQualityDTO.class);
		double baseProduction =  paperQualityDTO.getPaperPrice() * MULTIPLEBASEPRODUCTION;
		return baseProduction;
	}
	private Book setBook(BookDTO bookDTO) {
		Book book = new Book();
		//set author
		Author author = new Author();
		author.setAuthorId(bookDTO.getAuthor().getAuthorId());
		book.setAuthor(author);		
		//set category
		Category category = new Category();
		category.setCategoryId(bookDTO.getCategory().getCategoryId());
		book.setCategory(category);
		//set publisher
		Publisher publisher = new Publisher();
		publisher.setPublisherId(bookDTO.getPublisher().getPublisherId());
		book.setPublisher(publisher);
		
		book.setBookTitle(bookDTO.getBookTitle());
		
		
		book.setReleaseDate(bookDTO.getReleaseDate());
			
		return book;
	}
	
	private BookDTO setBookDTO(Book book){
		BookDTO bookDTO = new BookDTO();
			
		//set author
		AuthorDTO authorDTO = new AuthorDTO();
		authorDTO.setAuthorId(book.getAuthor().getAuthorId());
		authorDTO.setAge(book.getAuthor().getAge());
		authorDTO.setCountry(book.getAuthor().getCountry());
		authorDTO.setFirstName(book.getAuthor().getFirstName());
		authorDTO.setLastName(book.getAuthor().getLastName());
			
		//set category
		CategoryDTO categoryDTO = new CategoryDTO();
		categoryDTO.setCategoryId(book.getCategory().getCategoryId());
		categoryDTO.setCategoryName(book.getCategory().getCategoryName());
			
		//set publisher
		PublisherDTO publisherDTO = new PublisherDTO();
		publisherDTO.setPublisherId(book.getPublisher().getPublisherId());
		publisherDTO.setCity(book.getPublisher().getCity());
		publisherDTO.setCountry(book.getPublisher().getCountry());
		publisherDTO.setPublisherAddress(book.getPublisher().getPublisherAddress());
		publisherDTO.setPublisherName(book.getPublisher().getPublisherName());			
		//set paper quality
		PaperQualityDTO paperQualityDTO = new PaperQualityDTO();
		paperQualityDTO.setPaperPrice(book.getPublisher().getPaperQuality().getPaperPrice());
		paperQualityDTO.setQualityId(book.getPublisher().getPaperQuality().getQualityId());
		paperQualityDTO.setQualityType(book.getPublisher().getPaperQuality().getQualityType());
			
		publisherDTO.setPaperQuality(paperQualityDTO);
		bookDTO.setBookTitle(book.getBookTitle());
		bookDTO.setBookPrice(book.getBookPrice());
		bookDTO.setBookId(book.getBookId());
		bookDTO.setAuthor(authorDTO);
		bookDTO.setCategory(categoryDTO);
		bookDTO.setPublisher(publisherDTO);
		bookDTO.setReleaseDate(book.getReleaseDate());
		bookDTO.setBaseProduction(book.getBaseProduction());
			
		return bookDTO;
	}

	@GetMapping("/readAll")
	public Map<String, Object> getAllBook(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Book> listBookEntity = bookRepository.findAll();
		List<BookDTO> listBookDTO = new ArrayList<BookDTO>();
		for(Book book : listBookEntity) {
			listBookDTO.add(setBookDTO(book));
		}
		
		result.put("Status", 200);
		result.put("Messasge", "Read data book succesful");
		result.put("Data", listBookDTO);
		return result;
	}
	
	@GetMapping("read/{id}")
	public Map<String, Object> getBookByID(@PathVariable(value = "id")Long bookId){
		Map<String, Object> result = new HashMap<String, Object>();
		Book book = bookRepository.findById(bookId).get();
		BookDTO bookDTO = setBookDTO(book);	
		result.put("Status", 200);
		result.put("Messasge", "Read data book by ID succesful");
		result.put("Data", bookDTO);
		return result;
	}
	
	@PostMapping("/create")
	public Map<String, Object> createBook(@Valid @RequestBody BookDTO bookDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		
		Book book = setBook(bookDTO);
		book.setBaseProduction(baseProductionCalculating(bookDTO.getPublisher().getPublisherId()));
		bookRepository.save(book);
		result.put("Status", 200);
		result.put("Messasge", "Insert data book succesful");
		result.put("Data", bookDTO);
		return result;
	}
	//update book
	@PutMapping("/update/{id}")
	public Map<String, Object> updateBook(@PathVariable(value = "id")Long bookId , @Valid @RequestBody BookDTO bookDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Book book = setBook(bookDTO);
		book.setBookId(bookId);
		bookRepository.save(book);
		result.put("Status", 200);
		result.put("Messasge", "Update data book succesful");
		result.put("Data", book);
		return result;

	}
	
	//delete book
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteBook(@PathVariable(value = "id")Long idBook){
		Map<String, Object> result = new HashMap<String, Object>();
		Book book = bookRepository.findById(idBook).get();
		bookRepository.delete(book);
		result.put("Status", 200);
		result.put("Message", "Delete category succesful");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}
	
	
}
