package com.bookstore.bookstorederi.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.bookstorederi.dtomodel.BookDTO;
import com.bookstore.bookstorederi.dtomodel.PaperQualityDTO;
import com.bookstore.bookstorederi.dtomodel.PublisherDTO;
import com.bookstore.bookstorederi.interfaces.CalculationBookBaseProductionInterface;
import com.bookstore.bookstorederi.model.Book;
import com.bookstore.bookstorederi.model.PaperQuality;
import com.bookstore.bookstorederi.model.Publisher;
import com.bookstore.bookstorederi.repositories.BookRepository;
import com.bookstore.bookstorederi.repositories.PaperQualityRepository;
import com.bookstore.bookstorederi.repositories.PublisherRepository;

@RestController
@RequestMapping("/api/book")
public class BookBaseProductionController implements CalculationBookBaseProductionInterface {
	@Autowired
	BookRepository bookRepository ;
	@Autowired
	PublisherRepository publisherRepository;
	
	@Autowired 
	PaperQualityRepository paperQualityRepository;
	ModelMapper modelMapper = new ModelMapper();
	
	public Double baseProductionCalculating(Long idPublisher) {
		Publisher publisher = publisherRepository.findById(idPublisher).get();
		PublisherDTO publisherDTO = modelMapper.map(publisher, PublisherDTO.class);
		PaperQuality paperQuality = paperQualityRepository.findById(publisherDTO.getPaperQuality().getQualityId()).get();
		PaperQualityDTO paperQualityDTO = modelMapper.map(paperQuality, PaperQualityDTO.class);
		double baseProduction =  paperQualityDTO.getPaperPrice() * MULTIPLEBASEPRODUCTION;
		return baseProduction;
	}
	
	@PutMapping("/baseproductionCalculating")
	public Map<String, Object> baseProductionCalculation(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Book> listBook = bookRepository.findAll();
		List<BookDTO> listBookDTO = new ArrayList<BookDTO>();
		for(Book book : listBook) {
			BookDTO bookDTO = modelMapper.map(book, BookDTO.class);
			listBookDTO.add(bookDTO);
		}
		for(BookDTO bookDTO : listBookDTO) {
			Double baseProduction;
			baseProduction = baseProductionCalculating(bookDTO.getPublisher().getPublisherId());
			bookDTO.setBaseProduction(baseProduction);
			Book book = modelMapper.map(bookDTO, Book.class);
			bookRepository.save(book);
		}
		result.put("Status", 200);
		result.put("Message", "Update base production succesful");
		return result;
	}
}
