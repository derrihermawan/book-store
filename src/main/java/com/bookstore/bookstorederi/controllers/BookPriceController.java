package com.bookstore.bookstorederi.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.bookstorederi.dtomodel.BookDTO;
import com.bookstore.bookstorederi.interfaces.CalculationBookPriceInterface;
import com.bookstore.bookstorederi.model.Book;
import com.bookstore.bookstorederi.repositories.BookRepository;

@RestController
@RequestMapping("/api/book")
public class BookPriceController implements CalculationBookPriceInterface{
	@Autowired
	BookRepository booksRepository;
	ModelMapper modelMapper = new ModelMapper();
	@PutMapping("/pricecalculation")
	public Map<String, Object> priceCalculation(){
		List<Book> listBook = booksRepository.findAll();
		List<BookDTO> listBookDTO = new ArrayList<BookDTO>();
		for(Book book : listBook) {
			BookDTO bookDTO = modelMapper.map(book, BookDTO.class);
			listBookDTO.add(bookDTO);
		}
		for(BookDTO bookDTO : listBookDTO) {
			Double bookPrice = priceCalculating(bookDTO.getReleaseDate(),bookDTO.getBaseProduction());
			bookDTO.setBookPrice(bookPrice);
			Book book = modelMapper.map(bookDTO, Book.class);
			booksRepository.save(book);
		}
		
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Calculating price succesfull");
		
		return result;
	}
	
	public Double priceCalculating(Date date, Double baseProduction) {
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int releaseDateYear = calendar.get(Calendar.YEAR);
		Double bookPrice;
		if(currentYear == releaseDateYear) {
			bookPrice = MULTIPLE_BOOKPRICE_CURRENTYEAR * baseProduction;
		} 
		else if((currentYear - releaseDateYear) <= MAXYEAR_AGO_RELEASEDATE){
			bookPrice = MULTIPLE_BOOKPRICE_THREEYEAR_AGO * baseProduction;
		}
		else {
			bookPrice = MULTIPLE_BOOKPRICE_MORETHANTHREEYEAR_AGO * baseProduction;
		}
		return bookPrice;
	}
}
