package com.bookstore.bookstorederi.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.bookstorederi.dtomodel.CategoryDTO;
import com.bookstore.bookstorederi.model.Category;
import com.bookstore.bookstorederi.repositories.CategoryRepository;

@RestController
@RequestMapping("/api/category")
public class CategoryController {
	@Autowired
	CategoryRepository categoryRepository;
	//read all
	@GetMapping("/readAll")
	public Map<String, Object> getAllCategory(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Category> listCategoryEntity = categoryRepository.findAll();
		
		List<CategoryDTO> listCategoryDTO = new ArrayList<CategoryDTO>();
		for(Category category : listCategoryEntity) {
			CategoryDTO categoryDTO = new CategoryDTO();
			categoryDTO.setCategoryId(category.getCategoryId());
			categoryDTO.setCategoryName(category.getCategoryName());
			listCategoryDTO.add(categoryDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read data category succesful");
		result.put("Data", listCategoryDTO);
		return result;
	}
	
	//create category
	@PostMapping("/create")
	public Map<String, Object> createCategory(@Valid @RequestBody CategoryDTO categoryDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Category category = new Category();
		category.setCategoryName(categoryDTO.getCategoryName());
		result.put("Status", 200);
		result.put("Message", "Create category succesful");
		result.put("Data", categoryRepository.save(category));
		return result;
	}
	
	//update category
	@PutMapping("/update/{id}")
	public Map<String, Object> updateCategory(@PathVariable(value = "id")Long categoryId,@Valid @RequestBody CategoryDTO categoryDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		
		Category category = categoryRepository.findById(categoryId).get();
		category.setCategoryName(categoryDTO.getCategoryName());
		result.put("Status", 200);
		result.put("Message", "update category succesful");
		result.put("Data", categoryRepository.save(category));
		return result;
	}
	
	//delete category
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteCategory(@PathVariable(value = "id")Long categoryId){
		Map<String, Object> result = new HashMap<String, Object>();
		Category category = categoryRepository.findById(categoryId).get();
		categoryRepository.delete(category);
		result.put("Status", 200);
		result.put("Message", "Delete category succesful");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}
}
