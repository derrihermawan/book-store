package com.bookstore.bookstorederi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookstorederiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookstorederiApplication.class, args);
	}

}
