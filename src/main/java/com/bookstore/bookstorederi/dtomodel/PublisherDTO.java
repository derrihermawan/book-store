package com.bookstore.bookstorederi.dtomodel;

import com.bookstore.bookstorederi.model.PaperQuality;

public class PublisherDTO {
	private Long publisherId;
	private PaperQualityDTO paperQuality;
	private String city;
	private String country;
	private String publisherAddress;
	private String publisherName;
	
	public PublisherDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public PublisherDTO(Long publisherId, PaperQualityDTO paperQuality, String city, String country,
			String publisherAddress, String publisherName) {
		super();
		this.publisherId = publisherId;
		this.paperQuality = paperQuality;
		this.city = city;
		this.country = country;
		this.publisherAddress = publisherAddress;
		this.publisherName = publisherName;
	}

	public Long getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(Long publisherId) {
		this.publisherId = publisherId;
	}

	public PaperQualityDTO getPaperQuality() {
		return paperQuality;
	}

	public void setPaperQuality(PaperQualityDTO paperQuality) {
		this.paperQuality = paperQuality;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPublisherAddress() {
		return publisherAddress;
	}

	public void setPublisherAddress(String publisherAddress) {
		this.publisherAddress = publisherAddress;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}
	
	
}
