package com.bookstore.bookstorederi.dtomodel;

public class PaperQualityDTO {
	private Long qualityId;
	private String qualityType;
	private Double paperPrice;
	
	public PaperQualityDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public PaperQualityDTO(Long qualityId, String qualityType, Double paperPrice) {
		super();
		this.qualityId = qualityId;
		this.qualityType = qualityType;
		this.paperPrice = paperPrice;
	}

	public Long getQualityId() {
		return qualityId;
	}

	public void setQualityId(Long qualityId) {
		this.qualityId = qualityId;
	}

	public String getQualityType() {
		return qualityType;
	}

	public void setQualityType(String qualityType) {
		this.qualityType = qualityType;
	}

	public Double getPaperPrice() {
		return paperPrice;
	}

	public void setPaperPrice(Double paperPrice) {
		this.paperPrice = paperPrice;
	}
	
	
}
