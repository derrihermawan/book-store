package com.bookstore.bookstorederi.dtomodel;

import java.util.Date;

import com.bookstore.bookstorederi.model.Author;
import com.bookstore.bookstorederi.model.Category;
import com.bookstore.bookstorederi.model.Publisher;

public class BookDTO {
	private Long bookId;
	private AuthorDTO author;
	private CategoryDTO category;
	private PublisherDTO publisher;
	private String bookTitle;
	private Date releaseDate;
	private Double baseProduction;
	private Double bookPrice;
	
	public BookDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public BookDTO(Long bookId, AuthorDTO author, CategoryDTO category, PublisherDTO publisher, String bookTitle,
			Date releaseDate, Double baseProduction, Double bookPrice) {
		super();
		this.bookId = bookId;
		this.author = author;
		this.category = category;
		this.publisher = publisher;
		this.bookTitle = bookTitle;
		this.releaseDate = releaseDate;
		this.baseProduction = baseProduction;
		this.bookPrice = bookPrice;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public AuthorDTO getAuthor() {
		return author;
	}

	public void setAuthor(AuthorDTO author) {
		this.author = author;
	}

	public CategoryDTO getCategory() {
		return category;
	}

	public void setCategory(CategoryDTO category) {
		this.category = category;
	}

	public PublisherDTO getPublisher() {
		return publisher;
	}

	public void setPublisher(PublisherDTO publisher) {
		this.publisher = publisher;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Double getBaseProduction() {
		return baseProduction;
	}

	public void setBaseProduction(Double baseProduction) {
		this.baseProduction = baseProduction;
	}

	public Double getBookPrice() {
		return bookPrice;
	}

	public void setBookPrice(Double bookPrice) {
		this.bookPrice = bookPrice;
	}
	
	
	
}
