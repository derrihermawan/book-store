package com.bookstore.bookstorederi.dtomodel;

public class AuthorDTO {
	private Long authorId;
	private String country;
	private String firstName;
	private String lastName;
	private Integer age;
	
	public AuthorDTO() {
		// TODO Auto-generated constructor stub
	}
	public AuthorDTO(Long authorId, String country, String firstName, String lastName, Integer age) {
		super();
		this.authorId = authorId;
		this.country = country;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
	
	
	
	
}
