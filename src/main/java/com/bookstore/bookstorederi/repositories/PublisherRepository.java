package com.bookstore.bookstorederi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bookstore.bookstorederi.model.Publisher;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long>{

}
