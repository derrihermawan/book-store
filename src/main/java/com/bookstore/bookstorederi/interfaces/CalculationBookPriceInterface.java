package com.bookstore.bookstorederi.interfaces;

import java.util.Date;

public interface CalculationBookPriceInterface {
	Double MULTIPLE_BOOKPRICE_CURRENTYEAR = 1.5;
	Double MULTIPLE_BOOKPRICE_THREEYEAR_AGO = 1.3;
	Double MULTIPLE_BOOKPRICE_MORETHANTHREEYEAR_AGO = 1.2;
	Double MAXYEAR_AGO_RELEASEDATE = 3.0;
	Double priceCalculating(Date date, Double baseProduction);
	
}
