package com.bookstore.bookstorederi.interfaces;

public interface CalculationBookBaseProductionInterface {
	Double MULTIPLEBASEPRODUCTION = 1.2;
	
	Double baseProductionCalculating(Long idPublisher);
}
